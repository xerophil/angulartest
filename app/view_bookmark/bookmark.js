'use strict';

angular.module('angularity.bookmark', ['ngRoute', 'ngResource'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/bookmark', {
            templateUrl: 'view_bookmark/bookmark.html',
            controller: 'BookmarkCtrl',
            controllerAs: 'bookmarkCtrl'
        });
    }])

    .factory('AccountService', ['$resource', function ($resource) {
        return $resource("http://localhost:8080/accounts/:accid", {id: "@_id"},
            {
                'index': {method: 'GET', isArray: true}
            }
        );
    }])

    .factory('BookmarkService', ['$resource', function ($resource) {
        return $resource("http://localhost:8080/accounts/:accid/bookmarks/:bookmarkid", {id: "@_id"},
            {
                'create': {method: 'POST'},
                'index': {method: 'GET', isArray: true},
                'show': {method: 'GET', isArray: false},
                'update': {method: 'PUT'},
                'destroy': {method: 'DELETE'}
            }
        );
    }])

    .controller('BookmarkCtrl', ['AccountService', 'BookmarkService', function (accountApi, bookmarkApi) {
        var ctrl = this;
        ctrl.accounts = [];
        ctrl.active = {};

        ctrl.isActive = function (account) {
            return ctrl.active.id === account.id;
        };
        ctrl.setActive = function (newActive) {
            ctrl.active = newActive
            ctrl.loadBookmarks(newActive)
        };

        ctrl.loadData = function () {
            accountApi.index(function (data) {
                console.log("got accounts", data);
                ctrl.accounts = data;
                ctrl.active = data[0];
            });
        };

        ctrl.bookmarks = [];
        ctrl.preview = {};

        ctrl.loadBookmarks = function (account) {
            console.log("reloading bookmark data for account", account);
            bookmarkApi.index({accid: account.id}, function (data) {
                console.log("got data", data);
                account.bookmarks = data;
            });
        };



        ctrl.submitBookmark = function (account) {
            console.log("sending stuff...", ctrl.preview);
            bookmarkApi.create({accid: account.id}, ctrl.preview, function (data) {
                ctrl.loadBookmarks(account);
                ctrl.preview = {};
            });
        };

        ctrl.loadData();

    }])


    .directive('bookmarkForm', function () {
        return {
            restrict: 'E',
            templateUrl: 'bookmark-form.html',
            controller: 'BookmarkController',
            controllerAs: 'bookmarkCtrl'
        };
    })

    .directive('bookmarkTabs', function () {
        return {
            restrict: 'E',
            templateUrl: 'bookmark-tabs.html',
            controller: 'AccountController',
            controllerAs: 'accountCtrl'
        };
    });
;