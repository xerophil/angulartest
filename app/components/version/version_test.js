'use strict';

describe('angularity.version module', function() {
  beforeEach(module('angularity.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
