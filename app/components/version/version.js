'use strict';

angular.module('angularity.version', [
  'angularity.version.interpolate-filter',
  'angularity.version.version-directive'
])

.value('version', '0.1');
