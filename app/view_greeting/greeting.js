'use strict';

angular.module('angularity.greeting', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/greeting', {
            templateUrl: 'view_greeting/greeting.html',
            controller: 'GreetingCtrl',
            controllerAs: 'greetingCtrl'
        });
    }])

    .controller('GreetingCtrl', ['$http', function ($http) {
        var ctrl = this;
        ctrl.greetings = [];
        ctrl.greetingModel = {};

        ctrl.reloadGreetings = function () {
            console.log("reloading data...");
            $http.get('http://localhost:8080/greetings').success(function (data) {
                ctrl.greetings = data;
                console.log("done. got " + data.length + " elements", data);
            });
        };

        ctrl.submitGreeting = function () {
            console.log("sending stuff...");
            $http.post('http://localhost:8080/greetings?name=' + this.greetingModel.content, ctrl.greetingModel).success(function (data) {
                ctrl.reloadGreetings();
            });
            this.greetingModel = {};
        };

        ctrl.deleteGreeting = function(id) {
            console.log("Deleting ", id);
            $http.delete('http://localhost:8080/greetings/'+id).success(function (data) {
                ctrl.reloadGreetings();
            });
        };

        ctrl.reloadGreetings();

    }]);