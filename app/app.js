'use strict';

// Declare app level module which depends on views, and components
angular.module('angularity', [
  'ngRoute',
  'angularity.greeting',
  'angularity.bookmark',
  'angularity.version'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/bookmark'});
}]);
